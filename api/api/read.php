<?php
    include_once '../config/database.php';
    
    include_once '../class/estabelecimento.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Estabelecimento($db);

    $pesquisa = (!empty($_POST["pesquisa"])) ? $_POST["pesquisa"] : null;

    $stmt = $items->getEstabelecimentos($pesquisa);
    $itemCount = $stmt->rowCount();


    if($itemCount > 0){
        
        $estabelecimentoArr = array();
        $estabelecimentoArr["body"] = array();
        $estabelecimentoArr["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "titulo" => $titulo,
                "endereco" => $endereco
            );

            array_push($estabelecimentoArr["body"], $e);
        }
        echo json_encode($estabelecimentoArr['body']);
    }

    else{
        http_response_code(404);
        echo json_encode(
            []
        );
    }
?>