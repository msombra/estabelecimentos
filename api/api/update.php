<?php
    include_once '../config/database.php';
    include_once '../class/estabelecimento.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Estabelecimento($db);
    
    $data = json_decode(file_get_contents("php://input"));
    
    $item->id = $_POST["id"];
    
    $item->titulo = $_POST["titulo"];
    $item->endereco = $_POST["endereco"];
    
    if($item->updateEstabelecimento()){
        echo json_encode("Estabelecimento salvo.");
    } else{
        echo json_encode("Não foi possível atualizar o estabelecimento.");
    }
?>