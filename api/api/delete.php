<?php
    include_once '../config/database.php';
    include_once '../class/estabelecimento.php';
    
    $database = new Database();
    $db = $database->getConnection();
    
    $item = new Estabelecimento($db);
    
    
    $item->id = $_POST["id"];
    
    if($item->deleteEstabelecimento()){
        echo json_encode("Estabelecimento deletado");
    } else{
        echo json_encode("Não foi possível deletar o estabelecimento");
    }
?>