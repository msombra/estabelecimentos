<?php

    include_once '../config/database.php';
    include_once '../class/utils.php';

    
    
    $database = new Database();
    $db = $database->getConnection();

    $Utils = new Utils($db);
    
    $data = json_decode(file_get_contents('php://input'));

    $login = $Utils->enter($data->username, $data->password);

    if($login === false) {
        echo json_encode(['success' => false, 'token' => null]);
    } else {
        echo json_encode(['success' => true, 'token' => $login]);
    }
?>