<?php
    include_once '../config/database.php';
    include_once '../class/estabelecimento.php';

    $database = new Database();
    $db = $database->getConnection();

    $item = new Estabelecimento($db);

    $data = json_decode(file_get_contents("php://input"));

    $item->titulo = $_POST["titulo"];
    $item->endereco = $_POST["endereco"];

    if($item->createEstabelecimento($item)){
        echo json_encode('Estabelecimento criado');
    } else{
        echo json_encode('Não foi possível criar o estabelecimento');
    }
?>