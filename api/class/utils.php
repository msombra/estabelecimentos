<?php
class Utils
{
    private $conn;
    public function __construct($db = null)
    {
        $this->conn = $db;
    }
    public function enter($user, $password)
    {
        $retorno = false;

        $q = $this->conn->prepare("SELECT * FROM usuarios WHERE username=:user LIMIT 1");

        $q->bindValue(':user', $user);
        $q->execute();

        if ($q->rowCount() > 0){
            $row = $q->fetch(PDO::FETCH_ASSOC);

            if($row['password'] == sha1($password)) {
              return $this->gerarJwt($row['id']);
            }
        }

        return false;
    }

    public function gerarJwt($idUser)
    {
        $header = ['alg' => 'HS256', 'typ' => 'JWT'];
        $header = json_encode($header);
        $header = base64_encode($header);

        $payload = ['id' => $idUser];

        $payload = json_encode($payload);
        $payload = base64_encode($payload);

        $signature = hash_hmac('sha256', "$header.$payload", 'kasj918ndq3r3q', true);
        $signature = base64_encode($signature);

        return "$header.$payload.$signature";
    }

    public static function validarJwt($token)
    {
        $part = explode(".", $token);
        $header = $part[0];
        $payload = $part[1];
        $signature = $part[2];

        $valid = hash_hmac('sha256', "$header.$payload", 'kasj918ndq3r3q', true);
        $valid = base64_encode($valid);

        if ($signature == $valid)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

?>