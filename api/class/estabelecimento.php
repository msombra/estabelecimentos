<?php
    class Estabelecimento{
        private $conn;
        private $db_table = "estabelecimentos";
        
        public $id;
        public $titulo;
        public $endereco;

        public function __construct($db){
            $this->conn = $db;
        }

       
        public function getEstabelecimentos($pesquisa){

           $completeSql = (!empty($pesquisa)) ? ' LIKE %:pesquisa%';


            $sqlQuery = "SELECT id, titulo, endereco FROM " . $this->db_table . " WHERE 1=1 ".$completeSql;
            $stmt = $this->conn->prepare($sqlQuery);
            if(!empty($pesquisa)) {
                $stmt->execute(['pesquisa']);
            } else {
                $stmt->execute();
            }
            
            return $stmt;
        }

        public function createEstabelecimento(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        titulo = :titulo, 
                        endereco = :endereco";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->titulo=htmlspecialchars(strip_tags($this->titulo));
            $this->endereco=htmlspecialchars(strip_tags($this->endereco));
        
            $stmt->bindParam(":titulo", $this->titulo);
            $stmt->bindParam(":endereco", $this->endereco);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        public function updateEstabelecimento(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        titulo = :titulo, 
                        endereco = :endereco
                    WHERE 
                        id = :id
                    LIMIT 1";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->titulo=htmlspecialchars(strip_tags($this->titulo));
            $this->endereco=htmlspecialchars(strip_tags($this->endereco));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":titulo", $this->titulo);
            $stmt->bindParam(":endereco", $this->endereco);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        
        function deleteEstabelecimento(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ? LIMIT 1";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>

