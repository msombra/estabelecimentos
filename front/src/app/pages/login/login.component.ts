import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login.service';

import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms'
import { FormBuilder, FormGroup } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';

import { DomSanitizer, SafeUrl } from '@angular/platform-browser';


//import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username : string;
  password : string;
  alerta: string;

  constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router, private loginService: LoginService) { }
  
  ngOnInit(): void {
   
    this.alerta = '';
  }

 getSantizeUrl(url : string) { 
    return this.sanitizer.bypassSecurityTrustResourceUrl(url); 
}

  entrar() {
     this.loginService.entrar(this.username, this.password).subscribe((x) => {
        if(this.loginService.isLogado) {
         this.router.navigate(['/home'], {state: {data: {}}});
        } else {
           this.alerta = 'Dados não encontrados';
        }
      });
    

  }
}