import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { EstabelecimentoService } from 'src/app/services/estabelecimento.service';

import { LoginService } from 'src/app/services/login.service';

import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms'
import { FormBuilder, FormGroup } from '@angular/forms';

import { ActivatedRoute } from '@angular/router';

import { DomSanitizer, SafeUrl } from '@angular/platform-browser';


//import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-novo',
  templateUrl: './novo.component.html',
  styleUrls: ['./novo.component.css']
})
export class NovoComponent implements OnInit {

  estabelecimento = {} as Estabelecimento;
  estabelecimentos: Estabelecimento[];

  alerta: string;

  
  uploadForm: FormGroup;


  constructor(private sanitizer: DomSanitizer, private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router, private estabelecimentoService: EstabelecimentoService,  private loginService: LoginService) { }
  
  ngOnInit(): void {
    if( this.loginService.isLogado === false) {
      this.router.navigate(['/login'], {});
    }
    
    this.alerta = '';

    this.route.paramMap.subscribe(params => {
      var id = params.get('id');
      
      if(id !== null && id != undefined) {
        if(history.state.data !== undefined) {
            this.editEstabelecimento(history.state.data); 
        } else {
          this.router.navigate(['/'], {state: {data: {}}});
        }

       
      } 
    });

   
  }

 getSantizeUrl(url : string) { 
    return this.sanitizer.bypassSecurityTrustResourceUrl(url); 
}

  saveEstabelecimento(form: NgForm) {
     this.alerta = 'Estabelecimento salvo com sucesso.';
     var body = this;

    

    if (this.estabelecimento.id !== undefined) {
      this.estabelecimentoService.updateEstabelecimento(this.estabelecimento).subscribe(() => {
          this.router.navigate(['/'+this.alerta], {state: {data: {}}});

        this.cleanForm(form);
      });
    } else {

      this.estabelecimentoService.saveEstabelecimento(this.estabelecimento, this.uploadForm).subscribe((x) => {
        this.retornar();
   
        this.cleanForm(form);
      });
    }
  }

  retornar() {
    console.log('entrou');
    this.router.navigate(['/'+this.alerta], {state: {data: {}}});
  }

  


  getEstabelecimentos(id) {
    this.estabelecimentoService.getMovimentacoes(id).subscribe((movimentacoes: any[]) => {
      
    });
  }



  editEstabelecimento(estabelecimento: Estabelecimento) {
    this.estabelecimento = { ...estabelecimento };

    this.getEstabelecimentos(estabelecimento.id);
  }

  cleanForm(form: NgForm) {
    form.resetForm();
    this.estabelecimento = {} as Estabelecimento;
  }
}