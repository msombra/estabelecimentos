import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Estabelecimento } from 'src/app/models/estabelecimento';
import { EstabelecimentoService } from 'src/app/services/estabelecimento.service';
import { LoginService } from 'src/app/services/login.service';

import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ActivatedRoute } from '@angular/router';


//import { FormsModule } from '@angular/forms'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  estabelecimento = {} as Estabelecimento;
  estabelecimentos: Estabelecimento[];
  alerta: string;


  constructor(private route: ActivatedRoute, private router: Router, private estabelecimentoService: EstabelecimentoService, private loginService: LoginService) { }
  
  ngOnInit(): void {

    if( this.loginService.isLogado === false) {
      this.router.navigate(['/login'], {});
    }



    this.route.paramMap.subscribe(params => {
      var msg = params.get('msg');
      this.alerta = '';

      if(msg !== null) {
        this.alerta = msg;
      }
    });

    this.getEstabelecimentos();
  }


  getEstabelecimentos() {
    this.estabelecimentoService.getEstabelecimentos().subscribe((estabelecimentos: Estabelecimento[]) => {
      this.estabelecimentos = estabelecimentos;
    });
  }

  editEstabelecimento(estabelecimento) {
    this.router.navigate(['/novo/'+estabelecimento.id], {state: {data: estabelecimento}});
  }


  deleteEstabelecimento(estabelecimento: Estabelecimento) {
    this.alerta = '';
    this.estabelecimentoService.deleteEstabelecimento(estabelecimento).subscribe(() => {
      this.getEstabelecimentos();
    });
  }


}