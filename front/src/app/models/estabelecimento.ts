

export interface Estabelecimento {
  id: number;
  titulo: string;
  endereco: string;
}
