import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Estabelecimento } from '../models/estabelecimento';

import { FormBuilder, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class EstabelecimentoService {

  url = 'http://msombra.com.br/fortbrasil/api'; // api rest fake

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  // Headers
  /*httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
  }*/
  httpOptions = {
    headers: new HttpHeaders({  })
  }

  // Obtem todos os estabelecimentoros
  getEstabelecimentos(): Observable<Estabelecimento[]> {
    return this.httpClient.get<Estabelecimento[]>(this.url + '/read.php')
      .pipe(
        retry(2),
        catchError(this.handleError))
  }


  getMovimentacoes(id): Observable<Estabelecimento[]> {
    return this.httpClient.get<Estabelecimento[]>(this.url + '/movimentacoes.php?id='+id)
      .pipe(
        retry(2),
        catchError(this.handleError))
  }

  // Obtem um estabelecimentoro pelo id
  getEstabelecimentoById(id: number): Observable<Estabelecimento> {
    return this.httpClient.get<Estabelecimento>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError)
      )
  }

   saveMovimentacao(id, novo): Observable<Estabelecimento> {
        const formData = new FormData();

        formData.append('novo', novo);

        return this.httpClient.post<Estabelecimento>(this.url + '/inserir_movimentacao.php?id='+id, formData, this.httpOptions)
          .pipe(
            retry(0),
            catchError(this.handleError)
          )
      }


  // salva um estabelecimentor

      saveEstabelecimento(estabelecimento: Estabelecimento, uploadForm : FormGroup): Observable<Estabelecimento> {
        const formData = new FormData();

        formData.append('id', String(estabelecimento.id));
        formData.append('titulo', estabelecimento.titulo);
        formData.append('endereco', estabelecimento.endereco);


        return this.httpClient.post<Estabelecimento>(this.url + '/create.php', formData, this.httpOptions)
          .pipe(
            retry(0),
            catchError(this.handleError)
          )
      }

    

  // autualiza um estabelecimento
  updateEstabelecimento(estabelecimento: Estabelecimento): Observable<Estabelecimento> {
    return this.httpClient.put<Estabelecimento>(this.url + '/update.php?id=' + estabelecimento.id, JSON.stringify(estabelecimento), this.httpOptions)
      .pipe(
        retry(0),
        catchError(this.handleError)
      )
  }

  // deleta um estabelecimento
  deleteEstabelecimento(estabelecimento: Estabelecimento) {
    return this.httpClient.delete<Estabelecimento>(this.url + '/delete.php?id=' + estabelecimento.id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}