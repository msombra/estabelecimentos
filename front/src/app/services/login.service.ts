import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { FormBuilder, FormGroup } from '@angular/forms';

import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = 'http://msombra.com.br/fortbrasil/api'; // api rest fake

  // injetando o HttpClient
  constructor(private httpClient: HttpClient) { }

  
  httpOptions = {
    headers: new HttpHeaders({  })
  }

        entrar(username, password) {
          return this.httpClient.post<{token: string,success:  Boolean}>(this.url+'/login.php', {username, password}).pipe(tap(res => {
            console.log(res.success);

            if(res.success == true) {
              localStorage.setItem('access_token', res.token);
            } else {
              localStorage.setItem('access_token', "false");
            }
        }))
      }

      public get isLogado(): boolean{
        return localStorage.getItem('access_token') !==  null && localStorage.getItem('access_token') !==  "false";
      }

  // Manipulação de erros
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Erro ocorreu no lado do client
      errorMessage = error.error.message;
    } else {
      // Erro ocorreu no lado do servidor
      errorMessage = `Código do erro: ${error.status}, ` + `menssagem: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  };

}